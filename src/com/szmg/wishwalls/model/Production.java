package com.szmg.wishwalls.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.stereotype.Component;

@Component("production")
@Entity
@Table(name="tb_production")
public class Production implements Serializable{
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String menuLeafId;
	
	private String menuBaseId;
	
	private String name;
	
	private double price;
	
	private String description;
	
	private String img1Url;
	
	private String img2Url;
	
	private String img3Url;
	
	private String createName;
	
	private Date createTime;
	
	private String modifyName;
	
	private Date modifyTime;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")    
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid") 
	@Column(name="id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name="menu_leaf_id")
	public String getMenuLeafId() {
		return menuLeafId;
	}

	public void setMenuLeafId(String menuLeafId) {
		this.menuLeafId = menuLeafId;
	}

	@Column(name="menu_base_id")
	public String getMenuBaseId() {
		return menuBaseId;
	}

	public void setMenuBaseId(String menuBaseId) {
		this.menuBaseId = menuBaseId;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="price")
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="img1_url")
	public String getImg1Url() {
		return img1Url;
	}

	public void setImg1Url(String img1Url) {
		this.img1Url = img1Url;
	}

	@Column(name="img2_url")
	public String getImg2Url() {
		return img2Url;
	}

	public void setImg2Url(String img2Url) {
		this.img2Url = img2Url;
	}

	@Column(name="img3_url")
	public String getImg3Url() {
		return img3Url;
	}

	public void setImg3Url(String img3Url) {
		this.img3Url = img3Url;
	}

	@Column(name="create_name")
	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name="modify_name")
	public String getModifyName() {
		return modifyName;
	}

	public void setModifyName(String modifyName) {
		this.modifyName = modifyName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="modify_time")
	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
}


