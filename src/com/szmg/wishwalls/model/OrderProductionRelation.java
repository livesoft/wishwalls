package com.szmg.wishwalls.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.stereotype.Component;

/*
 * 实体类，对应着数据库中的表t_demo
 */
@Component("orderProductionRelation")
@Entity
@Table(name="tb_order_production_relation")
public class OrderProductionRelation implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String wishOrderId;
	
	private String productionId;
	
	private int voteCount;
	
	public OrderProductionRelation(){}
	
	public OrderProductionRelation(String wishOrderId,String productionId){
		this.wishOrderId = wishOrderId;
		this.productionId = productionId;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")    
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid") 
	@Column(name="id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name="wish_order_id")
	public String getWishOrderId() {
		return wishOrderId;
	}

	public void setWishOrderId(String wishOrderId) {
		this.wishOrderId = wishOrderId;
	}

	@Column(name="production_id")
	public String getProductionId() {
		return productionId;
	}

	public void setProductionId(String productionId) {
		this.productionId = productionId;
	}

	@Column(name="vote_count")
	public int getVoteCount() {
		return voteCount;
	}

	public void setVoteCount(int voteCount) {
		this.voteCount = voteCount;
	}
	
	
}
