package com.szmg.wishwalls.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.stereotype.Component;

@Component("voteDetail")
@Entity
@Table(name="tb_vote_detail")
public class VoteDetail implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String orderProductionRelationId;
	
	private String wechatId;
	
	private Date createTime;
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")    
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid") 
	@Column(name="id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name="order_production_relation_id")
	public String getOrderProductionRelationId() {
		return orderProductionRelationId;
	}

	public void setOrderProductionRelationId(String orderProductionRelationId) {
		this.orderProductionRelationId = orderProductionRelationId;
	}

	@Column(name="wechat_id")
	public String getWechatId() {
		return wechatId;
	}

	public void setWechatId(String wechatId) {
		this.wechatId = wechatId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
