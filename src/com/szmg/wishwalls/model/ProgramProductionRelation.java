package com.szmg.wishwalls.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.stereotype.Component;

/*
 * 实体类，对应着数据库中的表t_demo
 */
@Component("programProductionRelation")
@Entity
@Table(name="tb_program_production_relation")
public class ProgramProductionRelation implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String programId;
	
	private String productionId;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")    
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid") 
	@Column(name="id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name="program_id")
	public String getProgramId() {
		return programId;
	}
	
	public void setProgramId(String programId) {
		this.programId = programId;
	}

	@Column(name="production_id")
	public String getProductionId() {
		return productionId;
	}

	public void setProductionId(String productionId) {
		this.productionId = productionId;
	}
}
