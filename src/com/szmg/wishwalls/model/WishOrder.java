package com.szmg.wishwalls.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.stereotype.Component;

@Component("wishOrder")
@Entity
@Table(name="tb_wish_order")
public class WishOrder implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String programId;
	
	private String wechatId;
	
	private String wishDeclaration;
	
	private Date wishTime;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")    
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid") 
	@Column(name="id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name="program_id")
	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}

	@Column(name="wechat_id")
	public String getWechatId() {
		return wechatId;
	}

	public void setWechatId(String wechatId) {
		this.wechatId = wechatId;
	}

	@Column(name="wish_declaration")
	public String getWishDeclaration() {
		return wishDeclaration;
	}

	public void setWishDeclaration(String wishDeclaration) {
		this.wishDeclaration = wishDeclaration;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="wish_time")
	public Date getWishTime() {
		return wishTime;
	}

	public void setWishTime(Date wishTime) {
		this.wishTime = wishTime;
	}
}
