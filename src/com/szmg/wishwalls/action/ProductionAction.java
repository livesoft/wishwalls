package com.szmg.wishwalls.action;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.Action;
import com.szmg.wishwalls.model.Production;
import com.szmg.wishwalls.service.ProductionService;

@Controller("productionAction")
public class ProductionAction {
	private static final Logger log = LoggerFactory.getLogger(MenuAction.class);
	
	@Resource(name="productionService")
	private ProductionService productionService;
	
	private List<Production> productions;
	
	private String programCode;
	
	private String menuId;
	
	private String wishOrderId;
	
	public String queryProductionByProgramCode(){
		try{
			productions = productionService.queryProductionByProgramCode(programCode);
		}catch(Exception e){
			log.error("ProductionAction queryProductionByProgramCode error!", e);
		}
		return Action.SUCCESS;
	}
	
	public String queryProductionByMenuId(){
		try{
			productions = productionService.queryProductionByMenuId(programCode, menuId);
		}catch(Exception e){
			log.error("ProductionAction queryProductionByMenuId error!", e);
		}
		return Action.SUCCESS;
	}
	
	public String queryProductionByWishOrderId(){
		try{
			productions = productionService.queryProductionByWishOrderId(wishOrderId);
		}catch(Exception e){
			log.error("ProductionAction queryProductionByWishOrderId error!", e);
		}
		return Action.SUCCESS;
	}

	public List<Production> getProductions() {
		return productions;
	}

	public void setProductions(List<Production> productions) {
		this.productions = productions;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getWishOrderId() {
		return wishOrderId;
	}

	public void setWishOrderId(String wishOrderId) {
		this.wishOrderId = wishOrderId;
	}
}
