package com.szmg.wishwalls.action;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.Action;
import com.szmg.wishwalls.model.Menu;
import com.szmg.wishwalls.service.MenuService;

@Controller("menuAction")
public class MenuAction {
	private static final Logger log = LoggerFactory.getLogger(MenuAction.class);
	@Resource(name="menuService")
	private MenuService menuService;
	
	private String programCode;
	
	private String parentId;
	
	private List<Menu> menus;
	
	public String queryProgramBaseMenu(){
		try{
			menus = menuService.queryProgramBaseMenu(programCode);
		}catch(Exception e){
			log.error("MenuAction queryProgramBaseMenu error!",e);
		}
		return Action.SUCCESS;
	}
	
	public String queryMenuByParentId(){
		try{
			menus = menuService.queryMenuByParentId(programCode,parentId);
		}catch(Exception e){
			log.error("MenuAction queryMenuByParentId error!",e);
		}
		return Action.SUCCESS;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}
}
