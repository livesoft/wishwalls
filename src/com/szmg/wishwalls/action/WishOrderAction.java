package com.szmg.wishwalls.action;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.Action;
import com.szmg.wishwalls.dto.DiscussDto;
import com.szmg.wishwalls.dto.WishOrderDetailDto;
import com.szmg.wishwalls.model.Discuss;
import com.szmg.wishwalls.model.VoteDetail;
import com.szmg.wishwalls.model.WishOrder;
import com.szmg.wishwalls.service.WishOrderService;

@Controller("wishOrderAction")
public class WishOrderAction {
	private static final Logger log = LoggerFactory.getLogger(WishOrderAction.class);
	
	@Resource(name="wishOrderService")
	private WishOrderService wishOrderService;
	
	private WishOrder wishOrder;
	
	private WishOrderDetailDto wishOrderDto;
	
	private Discuss discuss;
	
	private VoteDetail voteDetail;
	
	private String wechatId;
	
	private String wishOrderId;
	
	private String parentId;
	
	private String productionId;
	
	private String[] productionIds;
	
	private List<DiscussDto> discussDtoList;
	
	private List<VoteDetail> voteDetailList;
	
	private String programCode;
	
	private boolean hadExists;
	
	public String saveWishOrder(){
		try{
			hadExists = wishOrderService.checkIsHadWishOrder(wishOrder,programCode);
			if(!hadExists){//当前许愿者没有许愿过，生成许愿单
				wishOrderService.saveWishOrder(wishOrder,programCode,productionIds);
			}
		}catch(Exception e){
			log.error("WishOrderAction saveWishOrder error!", e);
		}
		return Action.SUCCESS;
	}
	
	public String saveVoteDetail(){
		try{
			wishOrderService.saveVoteDetail(voteDetail,wishOrderId,productionId);
		}catch(Exception e){
			log.error("WishOrderAction saveWishOrder error!", e);
		}
		return Action.SUCCESS;
	}
	
	public String saveDiscuss(){
		try{
			wishOrderService.saveDiscuss(discuss,parentId);
		}catch(Exception e){
			log.error("WishOrderAction saveWishOrder error!", e);
		}
		return Action.SUCCESS;
	}
	
	public String queryWishOrderByWechatId(){
		try{
			wishOrder = wishOrderService.queryWishOrderByWechatId(wechatId);
		}catch(Exception e){
			log.error("WishOrderAction queryWishOrderByWechatId error!", e);
		}
		return Action.SUCCESS;
	}
	
	public String queryDiscussByWishOrderId(){
		try{
			discussDtoList = wishOrderService.queryDiscussByWishOrderId(wishOrderId);
		}catch(Exception e){
			log.error("WishOrderAction queryRootDiscussByWishOrderId error!", e);
		}
		return Action.SUCCESS;
	}
	
	public String queryVoteDetailsByRelationId(){
		try{
			voteDetailList = wishOrderService.queryVoteDetailsByRelationId(wishOrderId, productionId);
		}catch(Exception e){
			log.error("WishOrderAction queryVoteDetailsByRelationId error!", e);
		}
		return Action.SUCCESS;
	}

	public String queryWishOrderById(){
		try{
			wishOrderDto = wishOrderService.queryWishOrderById(wishOrderId);
		}catch(Exception e){
			log.error("WishOrderAction queryWishOrderById error!", e);
		}
		return Action.SUCCESS;
	}
	
	public WishOrder getWishOrder() {
		return wishOrder;
	}

	public void setWishOrder(WishOrder wishOrder) {
		this.wishOrder = wishOrder;
	}

	public Discuss getDiscuss() {
		return discuss;
	}

	public void setDiscuss(Discuss discuss) {
		this.discuss = discuss;
	}

	public VoteDetail getVoteDetail() {
		return voteDetail;
	}

	public void setVoteDetail(VoteDetail voteDetail) {
		this.voteDetail = voteDetail;
	}

	public String getWechatId() {
		return wechatId;
	}

	public void setWechatId(String wechatId) {
		this.wechatId = wechatId;
	}

	public String getWishOrderId() {
		return wishOrderId;
	}

	public void setWishOrderId(String wishOrderId) {
		this.wishOrderId = wishOrderId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getProductionId() {
		return productionId;
	}

	public void setProductionId(String productionId) {
		this.productionId = productionId;
	}

	public List<DiscussDto> getDiscussDtoList() {
		return discussDtoList;
	}

	public void setDiscussDtoList(List<DiscussDto> discussDtoList) {
		this.discussDtoList = discussDtoList;
	}

	public List<VoteDetail> getVoteDetailList() {
		return voteDetailList;
	}

	public void setVoteDetailList(List<VoteDetail> voteDetailList) {
		this.voteDetailList = voteDetailList;
	}
	
	public WishOrderDetailDto getWishOrderDto() {
		return wishOrderDto;
	}

	public void setWishOrderDto(WishOrderDetailDto wishOrderDto) {
		this.wishOrderDto = wishOrderDto;
	}

	public String[] getProductionIds() {
		return productionIds;
	}

	public void setProductionIds(String[] productionIds) {
		this.productionIds = productionIds;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public boolean isHadExists() {
		return hadExists;
	}

	public void setHadExists(boolean hadExists) {
		this.hadExists = hadExists;
	}
}
