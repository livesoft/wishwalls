package com.szmg.wishwalls.service;

import java.util.List;

import com.szmg.wishwalls.model.Menu;

public interface MenuService {
	/**
	 * 获取活动相关的所有根节点数据
	 * @param programCode
	 * @return
	 */
	public List<Menu> queryProgramBaseMenu(String programCode);
	
	/**
	 * 根据菜单父节点id，获取对应的子节点
	 * @param programCode
	 * @param parentId 
	 * @return
	 */
	public List<Menu> queryMenuByParentId(String programCode,String parentId);
}
