package com.szmg.wishwalls.service;

import java.util.List;

import com.szmg.wishwalls.dto.DiscussDto;
import com.szmg.wishwalls.dto.WishOrderDetailDto;
import com.szmg.wishwalls.model.Discuss;
import com.szmg.wishwalls.model.VoteDetail;
import com.szmg.wishwalls.model.WishOrder;

public interface WishOrderService {
	/**
	 * 保存心愿单对象和心愿单和产品的关系
	 * @param wishOrder
	 * @param programCode
	 * @param productionIds
	 */
	public void saveWishOrder(WishOrder wishOrder,String programCode,String[] productionIds);
	
	/**
	 * 保存投票明细信息
	 * @param voteDetail
	 * @param wishOrderId
	 * @param productionId
	 */
	public void saveVoteDetail(VoteDetail voteDetail,String wishOrderId,String productionId);
	
	/**
	 * 保存评论信息
	 * @param discuss
	 * @param discussParentId
	 */
	public void saveDiscuss(Discuss discuss,String discussParentId);
	
	/**
	 * 根据微信号获取心愿单
	 * @param wechatId
	 * @return
	 */
	public WishOrder queryWishOrderByWechatId(String wechatId);
	
	/**
	 * 根据心愿单id获取所有第一次评论，按时间顺序倒序排序
	 * @param wishOrderId
	 * @return
	 */
	public List<DiscussDto> queryDiscussByWishOrderId(String wishOrderId);
	
	/**
	 * 获取某个产品对应的投票者明细
	 * @param wishOrderId
	 * @param productionId
	 * @return
	 */
	public List<VoteDetail> queryVoteDetailsByRelationId(String wishOrderId,String productionId);
	
	/**
	 * 根据id获取心愿单相关记录
	 * @param wishOrderId
	 */
	public WishOrderDetailDto queryWishOrderById(String wishOrderId);
	
	/**
	 * 校验许愿者本次活动是否已经许过愿望
	 * @param wishOrder
	 * @param programCode
	 * @return
	 */
	public boolean checkIsHadWishOrder(WishOrder wishOrder,String programCode);
}
