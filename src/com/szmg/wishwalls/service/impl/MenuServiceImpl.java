package com.szmg.wishwalls.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.szmg.wishwalls.dao.MenuDao;
import com.szmg.wishwalls.model.Menu;
import com.szmg.wishwalls.service.MenuService;
import com.szmg.wishwalls.util.WishWalleException;


@Service("menuService")
public class MenuServiceImpl implements MenuService{
	@Resource(name="menuDao")
	private MenuDao menuDao;
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Menu> queryProgramBaseMenu(String programCode){
		try{
			return menuDao.queryProgramBaseMenu(programCode);
		}catch(Exception e){
			throw new WishWalleException("MenuServiceImpl queryProgramBaseMenu error!",e);
		}
	}
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Menu> queryMenuByParentId(String programCode,String parentId){
		try{
			return menuDao.queryMenuByParentId(programCode,parentId);
		}catch(Exception e){
			throw new WishWalleException("MenuServiceImpl queryMenuByParentId error!",e);
		}
	}
}
