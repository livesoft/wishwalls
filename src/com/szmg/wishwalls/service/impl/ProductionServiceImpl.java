package com.szmg.wishwalls.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.szmg.wishwalls.dao.ProductionDao;
import com.szmg.wishwalls.model.Production;
import com.szmg.wishwalls.service.ProductionService;
import com.szmg.wishwalls.util.WishWalleException;

@Service("productionService")
public class ProductionServiceImpl implements ProductionService{
	@Resource(name="productionDao")
	private ProductionDao productionDao;
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Production> queryProductionByProgramCode(String programCode){
		try{
			return productionDao.queryProductionByProgramCode(programCode);
		}catch(Exception e){
			throw new WishWalleException("ProductionServiceImpl queryProductionByProgramCode error!",e);
		}
	}
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Production> queryProductionByMenuId(String programCode,String menuId){
		try{
			return productionDao.queryProductionByMenuId(programCode,menuId);
		}catch(Exception e){
			throw new WishWalleException("ProductionServiceImpl queryProductionByMenuId error!",e);
		}
	}
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Production> queryProductionByWishOrderId(String wishOrderId){
		try{
			return productionDao.queryProductionByWishOrderId(wishOrderId);
		}catch(Exception e){
			throw new WishWalleException("ProductionServiceImpl queryProductionByWishOrderId error!",e);
		}
	}
}
