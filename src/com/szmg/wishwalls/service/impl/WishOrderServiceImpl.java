package com.szmg.wishwalls.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.szmg.wishwalls.dao.ProductionDao;
import com.szmg.wishwalls.dao.WishOrderDao;
import com.szmg.wishwalls.dto.DiscussDto;
import com.szmg.wishwalls.dto.ProductionDto;
import com.szmg.wishwalls.dto.VoteSortDto;
import com.szmg.wishwalls.dto.WishOrderDetailDto;
import com.szmg.wishwalls.model.Discuss;
import com.szmg.wishwalls.model.OrderProductionRelation;
import com.szmg.wishwalls.model.Production;
import com.szmg.wishwalls.model.Program;
import com.szmg.wishwalls.model.VoteDetail;
import com.szmg.wishwalls.model.WishOrder;
import com.szmg.wishwalls.service.WishOrderService;
import com.szmg.wishwalls.util.WishWalleException;

@Service("wishOrderService")
public class WishOrderServiceImpl implements WishOrderService{
	@Resource(name="wishOrderDao")
	private WishOrderDao wishOrderDao;
	
	@Resource(name="productionDao")
	private ProductionDao productionDao;
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveWishOrder(WishOrder wishOrder,String programCode,String[] productionIds){
		try{
			if(null == wishOrder || StringUtils.isBlank(wishOrder.getWechatId()) || StringUtils.isBlank(programCode)) return;
			Program program = wishOrderDao.queryProgramByCode(programCode);
			if(null == program) return;
			wishOrder.setProgramId(program.getId());
			wishOrderDao.saveOrUpdate(wishOrder);
			if(null != productionIds && productionIds.length > 0){//保存关联心愿单玉产品的关联关系
				for(String productionId : productionIds){
					OrderProductionRelation relation = new OrderProductionRelation(wishOrder.getId(),productionId);
					wishOrderDao.saveOrUpdate(relation);
				}
			}
		}catch(Exception e){
			throw new WishWalleException("WishOrderServiceImpl saveOrUpdateWishOrder error:",e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveVoteDetail(VoteDetail voteDetail,String wishOrderId,String productionId){
		try{
			if(null == voteDetail) return;
			//获取心愿单和产品的关联记录
			OrderProductionRelation relation = wishOrderDao.queryOrderProductionRelation(wishOrderId,productionId);
			if(null == relation) return;
			voteDetail.setOrderProductionRelationId(relation.getId());
			wishOrderDao.saveOrUpdate(voteDetail);
		}catch(Exception e){
			throw new WishWalleException("WishOrderServiceImpl saveVoteDetail error:",e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveDiscuss(Discuss discuss,String discussParentId){
		try{
			if(null == discuss) return;
			//默认设置为第一个评论，没有回复信息
			discuss.setParentId("0");
			if(!StringUtils.isBlank(discussParentId)){//校验关联的第一个评论是否存在
				Discuss parentDiscuss = wishOrderDao.queryDiscussById(discussParentId);
				if(null != parentDiscuss){
					discuss.setParentId(discussParentId);
				}
			}
			wishOrderDao.saveOrUpdate(discuss);
		}catch(Exception e){
			throw new WishWalleException("WishOrderServiceImpl saveDiscuss error:",e);
		}
	}
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public WishOrder queryWishOrderByWechatId(String wechatId){
		try{
			return wishOrderDao.queryWishOrderByWechatId(wechatId);
		}catch(Exception e){
			throw new WishWalleException("WishOrderServiceImpl queryWishOrderByWechatId error:",e);
		}
	}
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<DiscussDto> queryDiscussByWishOrderId(String wishOrderId){
		try{
			List<Discuss> dicussList = wishOrderDao.queryDiscussByWishOrderId(wishOrderId);
			if(CollectionUtils.isEmpty(dicussList)) return Collections.emptyList();
			List<Discuss> rootDicuss = new ArrayList<Discuss>(dicussList.size());
			//key为第一次评论的id
			Map<String,List<Discuss>> map = new HashMap<String,List<Discuss>>();
			for(Discuss discuss : dicussList){
				if("0".equals(discuss.getParentId())){//根评论
					rootDicuss.add(discuss);
				}else{//对所有第一个评论进行的回复进行划分
					List<Discuss> list = map.get(null != discuss.getParentId()?discuss.getParentId():"");
					if(CollectionUtils.isEmpty(list)){
						list = new ArrayList<Discuss>();
					}
					list.add(discuss);
					map.put(discuss.getParentId(), list);
				}
			}
			
			List<DiscussDto> discussDtoList = new ArrayList<DiscussDto>(rootDicuss.size());
			DiscussDto dto = null;
			for(Discuss discuss : rootDicuss){//对第一个评论的回复进行赋值
				dto = new DiscussDto();
				dto.setDiscuss(discuss);
				if(null == map.get(discuss.getId())){
					dto.setDiscussList(Collections.EMPTY_LIST);
				}else{
					dto.setDiscussList(map.get(discuss.getId()));
				}
				discussDtoList.add(dto);
			}
			return discussDtoList;
		}catch(Exception e){
			throw new WishWalleException("WishOrderServiceImpl queryDiscussByWishOrderId error:",e);
		}
	}
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<VoteDetail> queryVoteDetailsByRelationId(String wishOrderId,String productionId){
		try{
			return wishOrderDao.queryVoteDetailsByRelationId(wishOrderId,productionId);
		}catch(Exception e){
			throw new WishWalleException("WishOrderServiceImpl queryVoteDetailsByRelationId error:",e);
		}
	}
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public WishOrderDetailDto queryWishOrderById(String wishOrderId){
		WishOrder order = wishOrderDao.queryWishOrderById(wishOrderId);
		if(null == order) return null;
		WishOrderDetailDto orderDto = new WishOrderDetailDto();
		orderDto.setWishOrder(order);
		orderDto.setDiscussList(wishOrderDao.queryDiscussByWishOrderId(wishOrderId));
		
		List<Production> productionList = productionDao.queryProductionByWishOrderId(wishOrderId);
		if(CollectionUtils.isEmpty(productionList)) return orderDto;
		
		 List<ProductionDto> productionDtos = new ArrayList<ProductionDto>(productionList.size());
		for(Production production : productionList){
			ProductionDto dto = new ProductionDto();
			dto.setProduction(production);
			dto.setVoteSortDtoList(wishOrderDao.queryWishOrderDto(order.getProgramId(), production.getId()));
			productionDtos.add(dto);
		}
		orderDto.setProductionDtoList(productionDtos);
		return orderDto;
	}
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public boolean checkIsHadWishOrder(WishOrder wishOrder,String programCode){
		if(null == wishOrder || StringUtils.isBlank(wishOrder.getWechatId()) || StringUtils.isBlank(programCode)) return true;
		WishOrder order = wishOrderDao.queryWishOrderByWechat(wishOrder, programCode);
		if(null == order) return false;
		return true;
	}
}
