package com.szmg.wishwalls.util;

public class WishWalleException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	public WishWalleException(String message) {
		super(message);
	}

	public WishWalleException(String message, Throwable cause) {
		super(message, cause);
	}

	public WishWalleException(Throwable cause) {
		super(cause);
	}
}
