package com.szmg.wishwalls.util;

import java.util.Random;

public class StringUtil {
	
	private static final Random rnd = new Random();
	private static final int NORMAL = -1;
	private static final int NUMBER = 0;
	private static final int SIZE = 3;
	private static final int[] MAX_MEMBER = { 10, 26, 26 };
	private static final int[] BASE_ASCII = { 48, 65, 97 };
	
	/**
     * 判断是否是有效的字符串，空串为无效串
     * 
     * @param str
     * @return
     */
    public static boolean isValidStr(String str) {
        return null != str && 0 < str.trim().length();
    }

    
    
    public static String trimNull(Object str){
    	
    	if(str==null){
    		return "";
    	}
    	return String.valueOf(str);
    }
    
    /** */
	/**
	 * 随机生成数字和字母组成的字符串。
	 * 
	 * @param length
	 *            字符串长度
	 * @param type
	 *            类型(NORMAL/NUMBER)
	 * @return 字符串
	 */
	private static String nextString(int length, int type) {
		char[] charArray = new char[length];
		int curType = type;
		for (int i = 0; i < length; i++) {
			if (type == NORMAL)
				curType = rnd.nextInt(SIZE);
			charArray[i] = (char) (rnd.nextInt(MAX_MEMBER[curType]) + BASE_ASCII[curType]);
		}
		return new String(charArray);
	}

	/** */
	/**
	 * 随机生成数字和字母组成的字符串。
	 * 
	 * @param length
	 *            字符串长度
	 * @return 字符串
	 */
	public static String nextString(int length) {
		return nextString(length, NORMAL);
	}

	/** */
	/**
	 * 随机生成数字组成的字符串。
	 * 
	 * @param length
	 *            字符串长度
	 * @return 字符串
	 */
	public static String nextNumber(int length) {
		return nextString(length, NUMBER);
	}
}
