package com.szmg.wishwalls.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.struts2.ServletActionContext;

public class BaseAction {
	
	protected HttpClient getOldHttpClient(String key){
		return (HttpClient)this.getHttpServletRequest().getSession().getAttribute(key);
	}
	
	protected HttpClient getNewHttpClient(String key){
		HttpClient httpclient = (HttpClient)this.getHttpServletRequest().getSession().getAttribute(key);
		if(null != httpclient){//如果存在旧的，先remove，再新建
			try{
				httpclient.getConnectionManager().shutdown();
				this.getHttpServletRequest().getSession().removeAttribute(key);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		httpclient = new DefaultHttpClient();
		this.getHttpServletRequest().getSession().setAttribute(key, httpclient);
		return httpclient;
	}
	
	protected void removeHttpclient(String key){
		this.getHttpServletRequest().getSession().removeAttribute(key);
	}
	protected HttpServletRequest getHttpServletRequest(){
		return ServletActionContext.getRequest();
	}
	
	protected HttpServletResponse getHttpServletResponse(){
		return ServletActionContext.getResponse();
	}
}
