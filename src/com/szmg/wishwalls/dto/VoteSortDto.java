package com.szmg.wishwalls.dto;

import java.io.Serializable;

import com.szmg.wishwalls.util.StringUtil;

public class VoteSortDto implements Serializable{
	private static final long serialVersionUID = 1L;

	//被支持者的微信号
	private String wechat;
	
	//投票的总记录数
	private int voteCount;
	
	//排名次序
	private int sort;

	public void makeValue(Object[] obj,int i){
		this.wechat = StringUtil.trimNull(obj[0]);
		if(null != obj[1]){
			this.voteCount = Integer.valueOf(obj[1].toString());
		}
		//排名次序，从1开始
		this.sort = i + 1;
	}
	
	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public int getVoteCount() {
		return voteCount;
	}

	public void setVoteCount(int voteCount) {
		this.voteCount = voteCount;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}
}
