package com.szmg.wishwalls.dto;

import java.io.Serializable;
import java.util.List;

import com.szmg.wishwalls.model.Discuss;
import com.szmg.wishwalls.model.WishOrder;

public class WishOrderDetailDto implements Serializable{
	private static final long serialVersionUID = 1L;

	private WishOrder wishOrder;
	
	private List<ProductionDto> productionDtoList;
	
	private List<Discuss> discussList;

	public WishOrder getWishOrder() {
		return wishOrder;
	}

	public void setWishOrder(WishOrder wishOrder) {
		this.wishOrder = wishOrder;
	}

	public List<ProductionDto> getProductionDtoList() {
		return productionDtoList;
	}

	public void setProductionDtoList(List<ProductionDto> productionDtoList) {
		this.productionDtoList = productionDtoList;
	}

	public List<Discuss> getDiscussList() {
		return discussList;
	}

	public void setDiscussList(List<Discuss> discussList) {
		this.discussList = discussList;
	}
}
