package com.szmg.wishwalls.dto;

import java.io.Serializable;
import java.util.List;

import com.szmg.wishwalls.model.Production;

public class ProductionDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Production production;
	
	//心愿单某个产品在本次获得中的排名
	private List<VoteSortDto> voteSortDtoList;
	
	public Production getProduction() {
		return production;
	}
	public void setProduction(Production production) {
		this.production = production;
	}
	public List<VoteSortDto> getVoteSortDtoList() {
		return voteSortDtoList;
	}
	public void setVoteSortDtoList(List<VoteSortDto> voteSortDtoList) {
		this.voteSortDtoList = voteSortDtoList;
	}
}
