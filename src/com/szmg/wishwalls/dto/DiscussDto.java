package com.szmg.wishwalls.dto;

import java.io.Serializable;
import java.util.List;

import com.szmg.wishwalls.model.Discuss;

public class DiscussDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//第一次评论的内容
	private Discuss Discuss;
	
	//后面针对第一条评论进行的回复，按时间顺序排序
	private List<Discuss> DiscussList;

	public Discuss getDiscuss() {
		return Discuss;
	}

	public void setDiscuss(Discuss discuss) {
		Discuss = discuss;
	}

	public List<Discuss> getDiscussList() {
		return DiscussList;
	}

	public void setDiscussList(List<Discuss> discussList) {
		DiscussList = discussList;
	}
}
