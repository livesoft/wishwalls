package com.szmg.wishwalls.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.szmg.wishwalls.dao.WishOrderDao;
import com.szmg.wishwalls.dto.VoteSortDto;
import com.szmg.wishwalls.model.Discuss;
import com.szmg.wishwalls.model.OrderProductionRelation;
import com.szmg.wishwalls.model.Program;
import com.szmg.wishwalls.model.VoteDetail;
import com.szmg.wishwalls.model.WishOrder;

@Repository("wishOrderDao")
public class WishOrderDaoImpl implements WishOrderDao{
	@Resource(name="hibernateTemplate")
	private HibernateTemplate hibernateTemplate;
	
	public void saveOrUpdate(Object entity){
		hibernateTemplate.saveOrUpdate(entity);
	}
	
	public WishOrder queryWishOrderByWechatId(String wechatId){
		StringBuilder sb = new StringBuilder();
		sb.append("	FROM WishOrder w WHERE w.wechatId = ?");
		List<WishOrder> list = hibernateTemplate.find(sb.toString(),new Object[]{wechatId});
		if(CollectionUtils.isEmpty(list)) return null;
		return list.get(0);
	}
	
	public List<Discuss> queryRootDiscussByWishOrderId(String wishOrderId){
		StringBuilder sb = new StringBuilder();
		sb.append("	FROM Discuss d WHERE d.parentId = '0' and d.wishOrderId = ? order by createTime asc");
		List<Discuss> list = hibernateTemplate.find(sb.toString(),new Object[]{wishOrderId});
		if(CollectionUtils.isEmpty(list)) return Collections.emptyList();
		return list;
	}
	
	public List<Discuss> queryDiscussByWishOrderId(String wishOrderId){
		StringBuilder sb = new StringBuilder();
		sb.append("	FROM Discuss d WHERE d.wishOrderId = ? order by createTime asc");
		List<Discuss> list = hibernateTemplate.find(sb.toString(),new Object[]{wishOrderId});
		if(CollectionUtils.isEmpty(list)) return Collections.emptyList();
		return list;
	}
	
	public List<VoteDetail> queryVoteDetailsByRelationId(String wishOrderId,String productionId){
		StringBuilder sb = new StringBuilder();
		sb.append("	FROM VoteDetail v");
		sb.append("	WHERE");
		sb.append("	EXISTS(");
		sb.append("		SELECT 1");
		sb.append("		FROM OrderProductionRelation opr");
		sb.append("		WHERE opr.wishOrderId = ?");
		sb.append("		  and opr.productionId = ?");
		sb.append("		  and opr.id = v.orderProductionRelationId");
		sb.append("	)");
		List<VoteDetail> list = hibernateTemplate.find(sb.toString(),new Object[]{wishOrderId,productionId});
		if(CollectionUtils.isEmpty(list)) return Collections.emptyList();
		return list;
	}
	
	public OrderProductionRelation queryOrderProductionRelation(String wishOrderId,String productionId){
		StringBuilder sb = new StringBuilder();
		sb.append("	FROM OrderProductionRelation opr");
		sb.append("	WHERE");
		sb.append("	 opr.wishOrderId = ?");
		sb.append("	 and opr.productionId = ?");
		return (OrderProductionRelation)hibernateTemplate.find(sb.toString(),new Object[]{wishOrderId,productionId});
	}
	
	public Discuss queryDiscussById(String discussId){
		return hibernateTemplate.get(Discuss.class, discussId);
	}
	
	public WishOrder queryWishOrderById(String wishOrderId){
		return hibernateTemplate.get(WishOrder.class, wishOrderId);
	}
	
	public Program queryProgramByCode(String code){
		StringBuilder sb = new StringBuilder();
		sb.append("	FROM Program p");
		sb.append("	WHERE");
		sb.append("	 p.code = ?");
		List<Program> list = hibernateTemplate.find(sb.toString(),new Object[]{code});
		if(CollectionUtils.isEmpty(list)) return null;
		return list.get(0);
	}
	
	public WishOrder queryWishOrderByWechat(WishOrder wishOrder,String programCode){
		StringBuilder sb = new StringBuilder();
		sb.append("	FROM WishOrder w WHERE w.wechatId = ? and exists(select 1 from Program p where p.code = ? and w.programId = p.id)");
		List<WishOrder> list = hibernateTemplate.find(sb.toString(),new Object[]{wishOrder.getWechatId(),programCode});
		if(CollectionUtils.isEmpty(list)) return null;
		return list.get(0);
	}
	
	public List<VoteSortDto> queryWishOrderDto(String programId,String produtionId){
		StringBuilder sb = new StringBuilder();
		sb.append(" select");
		sb.append("   	wo.wechatId,");
		sb.append("   	o.voteCount");
		sb.append(" from");
		sb.append(" 	WishOrder wo,OrderProductionRelation o ");
		sb.append(" where wo.id = o.wishOrderId");
		sb.append("   and o.productionId = ?");
		sb.append("   and wo.programId = ?");
		sb.append("   and o.voteCount > 0");
		sb.append(" order by o.voteCount desc");
		sb.append(" LIMIT 0,10");
		Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery(sb.toString());
		query.setParameter(0, produtionId);
		query.setParameter(1, programId);
		List<Object[]> list = query.list();
		if(CollectionUtils.isEmpty(list)) return Collections.emptyList();
		List<VoteSortDto> dtoList = new ArrayList<VoteSortDto>(list.size());
		VoteSortDto dto = null;
		for(int i=0;i<list.size();i++){
			dto = new VoteSortDto();
			dto.makeValue(list.get(i),i);
			dtoList.add(dto);
		}
		return dtoList;
	}
}
