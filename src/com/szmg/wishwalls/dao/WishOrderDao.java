package com.szmg.wishwalls.dao;

import java.util.List;

import com.szmg.wishwalls.dto.VoteSortDto;
import com.szmg.wishwalls.model.Discuss;
import com.szmg.wishwalls.model.OrderProductionRelation;
import com.szmg.wishwalls.model.Program;
import com.szmg.wishwalls.model.VoteDetail;
import com.szmg.wishwalls.model.WishOrder;


public interface WishOrderDao {
	
	public void saveOrUpdate(Object entity);
	
	/**
	 * 根据微信号获取心愿单
	 * @param wechatId
	 * @return
	 */
	public WishOrder queryWishOrderByWechatId(String wechatId);
	
	/**
	 * 根据心愿单id获取所有第一次评论，按时间顺序倒序排序
	 * @param wishOrderId
	 * @return
	 */
	public List<Discuss> queryRootDiscussByWishOrderId(String wishOrderId);
	
	/**
	 * 获取心愿单的所有评论记录
	 * @param wishOrderId
	 * @return
	 */
	public List<Discuss> queryDiscussByWishOrderId(String wishOrderId);
	
	/**
	 * 获取某个产品对应的投票者明细
	 * @param wishOrderId
	 * @param productionId
	 * @return
	 */
	public List<VoteDetail> queryVoteDetailsByRelationId(String wishOrderId,String productionId);
	
	/**
	 * 查询心愿单和产品的关联关系
	 * @param wishOrderId
	 * @param productionId
	 * @return
	 */
	public OrderProductionRelation queryOrderProductionRelation(String wishOrderId,String productionId);
	
	/**
	 * 根据评论id获取评论
	 * @param discussId
	 * @return
	 */
	public Discuss queryDiscussById(String discussId);
	
	/**
	 * 根据心愿单id获取心愿单
	 * @param wishOrderId
	 * @return
	 */
	public WishOrder queryWishOrderById(String wishOrderId);
	
	/**
	 * 根据活动code获取活动实体
	 * @param code
	 * @return
	 */
	public Program queryProgramByCode(String code);
	
	/**
	 * 根据微信号查询本次活动的心愿单
	 * @param wishOrder
	 * @param programCode
	 * @return
	 */
	public WishOrder queryWishOrderByWechat(WishOrder wishOrder,String programCode);
	
	/**
	 * 获取指定产品在本次活动中的前十名
	 * @param programId
	 * @param produtionId
	 * @return
	 */
	public List<VoteSortDto> queryWishOrderDto(String programId,String produtionId);
}
