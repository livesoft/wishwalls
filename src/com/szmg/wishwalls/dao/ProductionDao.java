package com.szmg.wishwalls.dao;

import java.util.List;

import com.szmg.wishwalls.model.Production;

public interface ProductionDao {
	/**
	 * 获取当前活动的所有产品信息
	 * @param programCode
	 * @return
	 */
	public List<Production> queryProductionByProgramCode(String programCode);
	
	/**
	 * 获取指定节点下的所有产品
	 * @param programCode
	 * @param menuId
	 * @return
	 */
	public List<Production> queryProductionByMenuId(String programCode,String menuId);
	
	/**
	 * 获取某个心愿单对应的产品
	 * @param programCode
	 * @param menuId
	 * @return
	 */
	public List<Production> queryProductionByWishOrderId(String wishOrderId);
}
