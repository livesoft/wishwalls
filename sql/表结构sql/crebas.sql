/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2014/5/25 20:43:32                           */
/*==============================================================*/


drop table if exists tb_company;

drop table if exists tb_discuss;

drop table if exists tb_menu;

drop table if exists tb_order_production_relation;

drop table if exists tb_production;

drop table if exists tb_program;

drop table if exists tb_program_production_relation;

drop table if exists tb_vote_detail;

drop table if exists tb_wish_order;

/*==============================================================*/
/* Table: tb_company                                            */
/*==============================================================*/
create table tb_company
(
   id                   varchar(36) not null,
   name                 varchar(128),
   public_account       varchar(64),
   mobile               varchar(36),
   address              varchar(256),
   create_name          varchar(64),
   create_time          timestamp,
   modify_name          varchar(64),
   modify_time          timestamp,
   state                varchar(1),
   primary key (id)
);

/*==============================================================*/
/* Table: tb_discuss                                            */
/*==============================================================*/
create table tb_discuss
(
   id                   varchar(36) not null,
   wish_order_id        varchar(36),
   parent_id            varchar(36),
   wechat_id            varchar(64),
   comment              varchar(256),
   create_time          timestamp,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_menu                                               */
/*==============================================================*/
create table tb_menu
(
   id                   varchar(36) not null,
   name                 varchar(64),
   create_name          varchar(64),
   create_time          timestamp,
   modify_name          varchar(64),
   modify_time          timestamp,
   parent_id            varchar(36),
   primary key (id)
);

/*==============================================================*/
/* Table: tb_order_production_relation                          */
/*==============================================================*/
create table tb_order_production_relation
(
   id                   varchar(36) not null,
   wish_order_id        varchar(36),
   production_id        varchar(36),
   vote_count           bigint,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_production                                         */
/*==============================================================*/
create table tb_production
(
   id                   varchar(36) not null,
   menu_base_id         varchar(36),
   menu_leaf_id         varchar(36),
   name                 varchar(256),
   price                double(10,2),
   description          varchar(1000),
   img1_url             varchar(64),
   img2_url             varchar(64),
   img3_url             varchar(64),
   create_name          varchar(64),
   create_time          timestamp,
   modify_name          varchar(64),
   modify_time          timestamp,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_program                                            */
/*==============================================================*/
create table tb_program
(
   id                   varchar(36) not null,
   company_id           varchar(36),
   code                 varchar(36),
   simple_introduction  varchar(128),
   title                varchar(128),
   description          varchar(1000),
   start_date           timestamp,
   end_date             timestamp,
   create_name          varchar(36),
   create_time          timestamp,
   modify_name          varchar(36),
   modify_time          timestamp,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_program_production_relation                           */
/*==============================================================*/
create table tb_program_production_relation
(
   id                   varchar(36) not null,
   program_id           varchar(36),
   production_id        varchar(36),
   primary key (id)
);

/*==============================================================*/
/* Table: tb_vote_detail                                        */
/*==============================================================*/
create table tb_vote_detail
(
   id                   varchar(36) not null,
   order_production_relation_id varchar(36),
   wechat_id            varchar(64),
   create_time          timestamp,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_wish_order                                         */
/*==============================================================*/
create table tb_wish_order
(
   id                   varchar(36) not null,
   program_id           varchar(36),
   wechat_id            varchar(64),
   wish_declaration     varchar(256),
   wish_time            timestamp,
   primary key (id)
);

